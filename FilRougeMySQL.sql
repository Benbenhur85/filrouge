CREATE TABLE Repertoire(
		id_chanson          int  not null AUTO_INCREMENT,
        titre_chanson         Varchar(30) not null ,
        duree_chanson         time ,
        date_titre            Date ,
        nom_auteur_chanson    Varchar(30) ,
        prenom_auteur_chanson Varchar(30) ,
        PRIMARY KEY (id_chanson)
)

CREATE TABLE Rencontre(
        numero_rencontre				int   not null AUTO_INCREMENT,
        nom_rencontre                   Varchar(30) ,
        date_debut_rencontre            Date ,
        date_fin_rencontre              Date ,
        lieu_rencontre                  Varchar(30) ,
        nombre_personne_rencontre       int ,
        periodicite_rencontre           Varchar(30) ,
        nom_organisateur_rencontre      Varchar(30) ,
        numero_telephone_rencontre      Varchar(30) ,
        fax_rencontre                   Varchar(30) ,
        email_rencontre                 Varchar(30) ,
        civilite_organisateur_rencontre Varchar(30) ,
        PRIMARY KEY (numero_rencontre )
)



CREATE TABLE Groupe(
        numero_groupe				int AUTO_INCREMENT not null ,
        nom_groupe					Varchar(30),
        pays_groupe					Varchar(30),
        region_groupe				Varchar(30)
        PRIMARY KEY (numero_groupe )
)


CREATE TABLE membres(
        numero_membre         int AUTO_INCREMENT not null ,
        nom_membre            Varchar(30) ,
        prenom_membre         Varchar(30) ,
        date_naissance_membre Date ,
        civilite_membre       Varchar(30) ,
        --telephone_membre      Varchar (30) ,
       -- fax_membre            Varchar (30) ,
        --email_membre          Varchar (30) ,
        --adresse_membre        Varchar (30) ,
        --code_postal_membre    Varchar (30) ,
		
        --ville_membre          Varchar (30) ,
        PRIMARY KEY (numero_membre)
        
)


CREATE TABLE est_responsable(
			id_respo int not null AUTO_INCREMENT,  
			nom_respo varchar(30),
			prenom_respo varchar(30),
			civilite_respo       Varchar(30) ,
			telephone_respo   Varchar(30) ,
			fax_respo       Varchar(30) ,
			email_respo         Varchar(30) ,
			adresse_respo        Varchar(30) ,
			code_postal_respo    Varchar(30) ,
			ville_respo          Varchar(30) ,
			numero_groupe_respo Int NOT NULL references groupe (numero_groupe),
			primary key (id_respo)
			)

CREATE TABLE  oeuvretype(
		id_oeuvre int not null AUTO_INCREMENT,  
        chanson_type Varchar(30) not null ,
        PRIMARY KEY (id_oeuvre)

)


CREATE TABLE appartient(
        
        numero_groupe_appart       Int not null references groupe (numero_groupe),
        numero_membre_appart        Int not null references membres (numero_membre),
        PRIMARY KEY (numero_groupe_appart ,numero_membre_appart )


)




CREATE TABLE Programme(
        date_participation      date ,
		heure_debut_participation time,
        heure_fin_participation time,
        numero_groupe_participation            Int not null references groupe (numero_groupe),
        numero_rencontre_participation         Int not null references rencontre (numero_rencontre),
        id_chanson_participation            int not null references Repertoire (id_chanson),
        PRIMARY KEY (numero_groupe_participation ,numero_rencontre_participation ,id_chanson_participation )
)


CREATE TABLE fait_partie(
        id_chanson_partie int not null references repertoire (id_chanson),
        chanson_type_partie  Varchar (30) not null  references oeuvretype (chanson_type),
        PRIMARY KEY (id_chanson_partie ,chanson_type_partie )
)



CREATE TABLE Pratique(
       
		nom_instrument_prat				varchar (30)	null, 
        type_specialit�_prat			Varchar (30)	null,
		numero_membre_prat				int				not null	references membres (numero_membre),
		numero_groupe_prat				int				not null	references groupe (numero_groupe), 
        PRIMARY KEY (numero_groupe_prat ,numero_membre_prat )
)


--entr�e des donn�es de la table r�pertoire

Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('Sunday Bloody Sunday','00:04:45','27/12/1979','Bono', null);
Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('With or Without You', '00:04:53','21/03/1974',null,null);
Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('One','00:04:36','12/03/1992', null, null);
Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('Highway to Hell','00:04:45',null,null,null)
Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('Back in Black','00:04:46','25/12/1980', null, null) 
Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('Angie','00:04:33','17/08/1973', null,null)
Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('Satisfaction','00:04:33','27/05/1965', null,null)
Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('Roxanne','00:03:12','15/04/1978','Sting',null)
Insert into Repertoire (titre_chanson, duree_chanson,date_titre,nom_auteur_chanson,prenom_auteur_chanson)
	values ('Message in a Bottle', '00:04:44','02/09/1979', null, null)

--entr�e des valeurs de la table oeuvretype

Insert into oeuvretype (chanson_type)
	values	('chant');
			('danse');



--entr�e des valeurs rencontre

Insert into Rencontre (nom_rencontre, date_debut_rencontre, date_fin_rencontre, lieu_rencontre, nombre_personne_rencontre, periodicite_rencontre, nom_organisateur_rencontre, numero_telephone_rencontre, fax_rencontre, email_rencontre, civilite_organisateur_rencontre)
	values ('Main Square Festival', '15/07/2015','25/12/2015','Paris',500000,'annuelle', 'Boucher Jean-Paul', '06 89 78 25 14','01 45 14 75 45','jeanboucher@mainsquare.fr', 'Monsieur')

Insert into Rencontre ( nom_rencontre, date_debut_rencontre, date_fin_rencontre, lieu_rencontre, nombre_personne_rencontre, periodicite_rencontre, nom_organisateur_rencontre, numero_telephone_rencontre, fax_rencontre, email_rencontre, civilite_organisateur_rencontre)
	values ('Ard�che Luna Festival', '15/08/2015','01/01/2015', 'Privas', 1000000, 'annuelle', 'Jean Singer', '04 52 65 85 74',null, 'jeansinger@luna.fr', 'Monsieur')


--entr�e des valeurs de la table groupe

Insert into groupe (nom_groupe, pays_groupe, region_groupe)
	values	( 'Police', 'Royaume-Uni', 'Bristol'),
			( 'ACDC','Australie', null),
			('U2','Irlande', 'Dublin'),
			('Rolling Stones', 'Royaume-Uni', 'Londres')



--entr�e des valeurs de la table membres

Insert into membres (nom_membre, prenom_membre, date_naissance_membre,civilite_membre)
	values	('Bono', null, '10/05/1960', 'Monsieur'),
			('The Edge', null, '08/08/1961','Monsieur'),
			('Jagger', 'Mick', null, 'Sir'),
			('Richards', 'Keith', '18/12/1943', 'Monsieur'),
			('Johnson','Brian', null, 'Sir'),
			('Sting', null, null, null),
			('Branson','Charles',null, 'Monsieur'),
			( 'Eastwood', 'Clint', null, 'Monsieur')


--entr�e des valeurs la table est_charge

Insert into appartient( numero_membre_appart,numero_groupe_appart)
	values	(1,3),
			(7,3),
			(4,2)
Insert into appartient (numero_membre_appart,numero_groupe_appart)
	values ( 6,1),
	( 5,3),
			
			(2,3),
			(8, 2) 

--entr�e des valeurs de la table programme

Insert into Programme (date_participation, heure_debut_participation, heure_fin_participation,numero_rencontre_participation,numero_groupe_participation, id_chanson_participation)
		values	('18/07/2015','15:14:00', '16:30:00', 1 ,3,1),
				( '18/07/2015','15:30:00','16:40:00', 1,3,2),
				( '18/07/2015', '15:14:00','16:50:00', 1,4,6),
				( '20/07/2015','16:45:00','17:30:00', 2, 1,7),
				( '30/08/2015','20:30:00','22:00:00', 2, 1,4)
Insert into Programme (date_participation, heure_debut_participation, heure_fin_participation,numero_rencontre_participation,numero_groupe_participation, id_chanson_participation)
		values( '18/07/2015','15:45:00', '16:30:00', 1 ,3,3),
			( '30/08/2015','20:40:00','22:00:00', 2, 1,5),
			( '20/07/2015','16:55:00','17:30:00', 2, 1,9),
			( '18/07/2015', '15:30:00','16:50:00', 1,4,7)
Insert into Programme (date_participation, heure_debut_participation, heure_fin_participation,numero_rencontre_participation,numero_groupe_participation, id_chanson_participation)
		values			( '18/07/2015', '15:30:00','16:50:00', 2,3,7)

--entr�e des valeurs de la table pratique

Insert into Pratique (nom_instrument_prat, type_specialit�_prat, numero_membre_prat, numero_groupe_prat)
	values	('guitare', 'guitariste', 2, 3),
			( 'guitare', 'guitariste', 4,4),
			(null, 'chant', 1,3),
			(null,'chant', 5,2),
			(null,'chant', 3, 4)
			
--entr�e des valeurs de la table fait partie		

Insert into fait_partie (id_chanson_partie,chanson_type_partie)
	values	(1, 'chant'),
			(8,'danse')