<?php 

function connect_BDD()
{
	$host = "localhost";
	$login = "root";
	$password = "";
	$base= "filrouge";

	try {
		
			$db = new PDO('mysql:host=' . $host . ';charset=utf8;dbname=' . $base,$login, $password);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


		return $db;
	} catch (Exeption $e) {

		print "Erreur : " . $e->getMessage() . "<br>";
		print "N° : " .$e->getCode();
		die ("Connexion au serveur impossible. <br>< a href= \"javascript : history.go(-1)\">BACK</a>");
	}
} 

 ?>