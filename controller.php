<?php 
error_reporting(E_ALL);

switch ($_REQUEST["action"]) {

  case "liste": 
    	require ("connect_bdd.php"); // appel le fichier pour la connexion à la BDD
  	  $db = connect_BDD(); // dans db on met le résultat de la fonction connect_BDD c'est à dire la connexion à la BDD
    	require("dao.php"); // appel le fichier ou se trouve les fonctions
      $model = liste_groupes(); // met dans la variable le résultat de la fonction liste_groupes
      echo json_encode($model); // écrit les données au format Json
      break;
    
case "details": // créé une liste des détails d'un groupe
      $id = $_GET["id"]; // envoi l'id du groupe dans la variable id
      require "connect_bdd.php";
      $db = connect_BDD();
      require "dao.php";
      $model = detail_groupes($id);
      echo json_encode($model);
      break;

  case "form_ajout": // Crée le formulaire de création d'un groupe
    
      require("formulaire_ajout.php");
      break;

  case "ajout": // Envoi les données du formulaire ajout
      require ("connect_bdd.php");
      $db = connect_BDD(); // récupère les données du formulaire pour envoi par la suite
      $nom = $_POST["nom"]; // récupère les données du formulaire pour envoi par la suite
      $pays = $_POST["pays"]; // récupère les données du formulaire pour envoi par la suite
      $region = $_POST["region"]; // récupère les données du formulaire pour envoi par la suite
      require ("script_ajouter.php");
      break;
                            
    case "modifier": // récupère les données d'un groupe à modifier
      $id = $_GET["id"]; // le nom de l'id doit correspondre ici et dans le script modifier
      require "connect_bdd.php";
      $db = connect_BDD();
      require "dao.php";
      $model = detail_groupes($id);
      echo json_encode($model);
      break;

    case "modifier2": // envoi les données d'un groupe à modifier
      require "connect_bdd.php";
      require("dao.php");
          $db = connect_BDD();
          $id = $_POST["numero_groupe"];
          $nom = $_POST["nom"];
          $pays=$_POST["pays"];
          $region=$_POST["region"];
          require("script_modifier.php");
          break;

case "modifierDetails": // envoi les données d'un groupe à modifier
      require "connect_bdd.php";
      require("dao.php");
          $db = connect_BDD();
          $id = $_POST["numero_groupe"];
          $nom = $_POST["nom"];
          $pays=$_POST["pays"];
          $region=$_POST["region"];
          require("script_modifier.php");
          break;
          
  case "supprimer": // script pour supprimer un groupe
    $id = $_REQUEST["id"];
    require "connect_bdd.php";
    $db = connect_BDD();
    require ("script_supprimer.php");
    break;
}

 ?>