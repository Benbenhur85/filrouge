<?php 

function liste_groupes() {

    $db=new PDO('mysql:host=localhost;dbname=filrouge;charset=utf8', "root", ""); // connexion à la BDD
    $result = $db->query ("select * from groupe"); // requête sur la table groupe dans une variable result

    $model = Array(); // création d'un tableau avec les données
    while ( $groupe = $result->fetch(PDO::FETCH_OBJ)  ) { // chaque ligne de la BDD devient un objet dans la variable groupe
        $model[] = $groupe; // chaque objet devient une ligne dans le tableau model
    }

    $result->closeCursor(); // vide la mémoire de result
    return $model; //retour la variable model pour le contrôleur
}

function detail_groupes($id) {

    $db=new PDO('mysql:host=localhost;dbname=filrouge;charset=utf8', "root", "");
    $result = $db->query ("select * from groupe where numero_groupe=" . $id);

    $model = $result->fetch(PDO::FETCH_OBJ);
    
    return $model;
}

?>