<?php 

try
{
$requete = $db->prepare("INSERT INTO groupe (nom_groupe, pays_groupe, region_groupe)  
						VALUES (:nom, :pays, :region)"); // requête insert

$requete->bindParam(":nom", $nom); // envoi des paramètres
$requete->bindParam(":pays", $pays); // envoi des paramètres
$requete->bindParam(":region", $region); // envoi des paramètres

$requete->execute();
}
catch (Exception $e)
{
echo "La création du groupe a échoué.";
}
 
?>