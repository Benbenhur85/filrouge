<?php 
try {
$requete = $db->prepare("UPDATE groupe set nom_groupe=:nom, pays_groupe=:pays, region_groupe=:region WHERE numero_groupe=:numero");
$requete->bindParam(":nom", $nom);
$requete->bindParam(":pays", $pays);
$requete->bindParam(":region", $region);
$requete->bindParam(":numero", $id); // le nom de l'id doit correspondre ici et dans le contôleur
$requete->execute();

}
catch (Exception $e)
{
echo "La modification du groupe a échoué.";
}
 ?>